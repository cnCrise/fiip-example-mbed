#include "./main.h"
#include "./ble/ble.h"

events::EventQueue event_queue;

int main() {
  printf("init.\r\n");
  ThisThread::sleep_for(10s);
  printf("start.\r\n");

  BleDemo demo_ble(BLE::Instance());
  demo_ble.init();

  event_queue.dispatch_forever();
  return 0;
}
