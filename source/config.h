#ifndef MY_CONFIG_H
#define MY_CONFIG_H
#include <stdint.h>

typedef struct _Config {
  uint8_t device_id[8];
} Config;
extern Config config;

#endif // MY_CONFIG_H
