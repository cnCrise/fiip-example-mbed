#ifndef MY_BLE_GAP_H
#define MY_BLE_GAP_H
#include "ble/BLE.h"

class GapDemo : private mbed::NonCopyable<GapDemo>,
                public ble::Gap::EventHandler {
#define MAX_ADVERTISING_PAYLOAD_SIZE 50
public:
  GapDemo();
  ~GapDemo();

  void init(ble::Gap &gap);
  void init_adv(void);
  void init_scan(void);

  void start_advertise();

  void start_scan();
  void update_connParas(ble::connection_handle_t connection_handle);

  void disconnect(ble::connection_handle_t connection_handle,
                  ble::local_disconnection_reason_t reason =
                      ble::local_disconnection_reason_t::USER_TERMINATION);

private:
  void onAdvertisingEnd(const ble::AdvertisingEndEvent &event) override;
  void onAdvertisingReport(const ble::AdvertisingReportEvent &event) override;
  void onScanTimeout(const ble::ScanTimeoutEvent &event) override;
  void onConnectionComplete(const ble::ConnectionCompleteEvent &event) override;
  void onDisconnectionComplete(
      const ble::DisconnectionCompleteEvent &event) override;

private:
  ble::Gap *_gap = nullptr;

  uint8_t _adv_buffer[MAX_ADVERTISING_PAYLOAD_SIZE];
  ble::AdvertisingDataBuilder _adv_data_builder;
};

extern GapDemo gap;

#endif // MY_BLE_GAP_H
