#ifndef MY_BLE_LINKS_H
#define MY_BLE_LINKS_H
#include "ble/BLE.h"

class BleLinks {
#define LINKS_LEN 64
public:
  typedef struct _link {
    int64_t handle;
    uint8_t mac[6];
    GattAttribute::Handle_t id_handle;
    GattAttribute::Handle_t rx_handle;
  } Link;

public:
  BleLinks();
  ~BleLinks();

  Link *get_link_by_handle(int64_t handle);
  Link *get_link_by_mac(const uint8_t mac[6]);
  Link *
  add_link(int64_t handle, const uint8_t mac[6],
           GattAttribute::Handle_t id_handle = GattAttribute::INVALID_HANDLE,
           GattAttribute::Handle_t rx_handle = GattAttribute::INVALID_HANDLE);
  void del_link(Link *link);

private:
  Link _links[LINKS_LEN];
  uint8_t _links_len = 0;
};

extern BleLinks blelinks;

#endif // MY_BLE_LINKS_H
