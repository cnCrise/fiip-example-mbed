#ifndef MY_BLE_GATT_SERVER_H
#define MY_BLE_GATT_SERVER_H
#include "../main.h"
#include "./links.h"
#include "ble/BLE.h"

class GattServerDemo : private mbed::NonCopyable<GattServerDemo>,
                       public ble::GattServer::EventHandler {
  const static uint16_t UNMP_SERVICE_UUID = 0x6460;
  const static uint16_t UNMP_ID_CHAR_UUID = 0x6466;
  const static uint16_t UNMP_RX_CHAR_UUID = 0x6461;

public:
  GattServerDemo();
  ~GattServerDemo() ;

  void init(ble::GattServer &gattserver);

private:
  virtual void onDataWritten(const GattWriteCallbackParams &params);

private:
  ReadOnlyGattCharacteristic<uint8_t[8]> _unmp_id;
  WriteOnlyGattCharacteristic<uint8_t[300]> _unmp_rx;
  uint8_t _rx_buf[300];
};

extern GattServerDemo gattserver;

#endif // MY_BLE_GATT_SERVER_H
