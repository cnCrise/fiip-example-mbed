#ifndef MY_BLE_H
#define MY_BLE_H
#include "../main.h"
#include "./gap.h"
#include "./gattclient.h"
#include "./gattserver.h"
#include "ble/BLE.h"
#include "pretty_printer.h"
#include <events/mbed_events.h>

class BleDemo {
public:
  BleDemo(BLE &ble) : _ble(ble) {}
  ~BleDemo() {}

  void init() {
    _ble.onEventsToProcess(
        makeFunctionPointer(this, &BleDemo::schedule_ble_events));
    ble_error_t error = _ble.init(this, &BleDemo::on_ble_init_complete);
    if (error) {
      print_error(error, "Error returned by BLE::init\r\n");
    }
  }

private:
  void schedule_ble_events(BLE::OnEventsToProcessCallbackContext *context) {
    event_queue.call(Callback<void()>(&context->ble, &BLE::processEvents));
  }
  void on_ble_init_complete(BLE::InitializationCompleteCallbackContext *event) {
    if (event->error) {
      print_error(event->error, "Error during the initialisation\r\n");
      return;
    }
    gattserver.init(_ble.gattServer());
    gattclient.init(_ble.gattClient());
    gap.init(_ble.gap());
    gap.start_advertise();
    event_queue.call_every(30s, &gap, &GapDemo::start_scan);
  }

private:
  BLE &_ble;
};

#endif // MY_BLE_H
