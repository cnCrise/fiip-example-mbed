#ifndef MY_BLE_GATT_CLIENT_H
#define MY_BLE_GATT_CLIENT_H
#include "../main.h"
#include "./gap.h"
#include "./links.h"
#include "ble/BLE.h"

class GattClientDemo : private mbed::NonCopyable<GattClientDemo>,
                       public GattClient::EventHandler {
  typedef GattClientDemo Self;
  const static uint16_t UNMP_SERVICE_UUID = 0x6460;
  const static uint16_t UNMP_ID_CHAR_UUID = 0x6466;
  const static uint16_t UNMP_RX_CHAR_UUID = 0x6461;

public:
  GattClientDemo();
  ~GattClientDemo();

  void init(ble::GattClient &gattclient);
  void start_discovery(ble::connection_handle_t connection_handle);

private:
  virtual void onAttMtuChange(ble::connection_handle_t connection_handle,
                              uint16_t attMtuSize);

private:
  void when_service_discovered(const DiscoveredService *discovered_service);
  void when_characteristic_discovered(
      const DiscoveredCharacteristic *discovered_characteristic);

  void when_service_discovery_ends(ble::connection_handle_t connection_handle);

  void when_characteristic_read(const GattReadCallbackParams *event);
  void when_characteristic_written(const GattWriteCallbackParams *event);

private:
  GattClient *_client = nullptr;
};

extern GattClientDemo gattclient;

#endif // MY_BLE_GATT_CLIENT_H
