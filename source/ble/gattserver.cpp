#include "./gattserver.h"
#include "../main.h"
#include "./links.h"
#include "ble/BLE.h"

GattServerDemo gattserver;

GattServerDemo::GattServerDemo()
    : _unmp_id(UNMP_ID_CHAR_UUID, &config.device_id),
      _unmp_rx(UNMP_RX_CHAR_UUID, &_rx_buf) {}
GattServerDemo::~GattServerDemo() {}

void GattServerDemo::init(ble::GattServer &gattserver) {
  GattCharacteristic *charTable[] = {&_unmp_id, &_unmp_rx};
  GattService example_service(UNMP_SERVICE_UUID, charTable, 2);
  gattserver.addService(example_service);
  gattserver.setEventHandler(this);
}

void GattServerDemo::onDataWritten(const GattWriteCallbackParams &params) {
  if (params.handle == _unmp_rx.getValueHandle()) {
    BleLinks::Link *link = blelinks.get_link_by_handle(params.connHandle);
    MBED_ASSERT(link != nullptr);
    printf("unmp rx:");
    for (int i = 0; i < params.len; i++) {
      printf("%02X", params.data[i]);
    }
    printf("\n");
  }
}
