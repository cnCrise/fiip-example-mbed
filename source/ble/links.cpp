#include "./links.h"
#include "ble/BLE.h"

BleLinks blelinks;

BleLinks::BleLinks() {}
BleLinks::~BleLinks() {}

BleLinks::Link *BleLinks::get_link_by_handle(int64_t handle) {
  for (int i = 0; i < _links_len; i++) {
    Link *link = &_links[i];
    if (link->handle == handle) {
      return link;
    }
  }
  return nullptr;
}
BleLinks::Link *BleLinks::get_link_by_mac(const uint8_t mac[6]) {
  for (int i = 0; i < _links_len; i++) {
    Link *link = &_links[i];
    if (memcmp(link->mac, mac, 6) == 0) {
      return link;
    }
  }
  return nullptr;
}
BleLinks::Link *BleLinks::add_link(int64_t handle, const uint8_t mac[6],
                                   GattAttribute::Handle_t id_handle,
                                   GattAttribute::Handle_t rx_handle) {
  if (_links_len >= LINKS_LEN) {
    return nullptr;
  }
  Link *link = &_links[_links_len++];
  link->handle = handle;
  memcpy(link->mac, mac, 6);
  link->id_handle = id_handle;
  link->rx_handle = rx_handle;
  return link;
}
void BleLinks::del_link(BleLinks::Link *link) {
  memcpy(link, &_links[--_links_len], sizeof(Link));
}
